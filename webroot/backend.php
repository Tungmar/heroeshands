<?php
declare(strict_types=1);

use Slim\App;
use App\Service\Controller;
use App\Service\Doctrine;
use App\Service\Slim;
use App\Service\SlimRoute;

define('APP_ROOT', dirname(__DIR__) . '/');

$container = require APP_ROOT . 'app/bootstrap.php';

$container->register(new Controller());
$container->register(new Doctrine());
$container->register(new Slim());
$container->register(new SlimRoute());

if (true === $container->get('develop_mode')) {
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Credentials: true');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    header('Access-Control-Allow-Headers: *');
}

$container->get(App::class)->run();
