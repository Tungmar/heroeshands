# Heroes Hands
A simple tool to help play board games over video stream. It simulates the players hands, hidden from each other.

## Install it on Uberspace

1. Go to https://dashboard.uberspace.de/register and create a new account.
2. Set a password, upload your public SSH key and login to the shell.
3. For easy access, create a link to where we place the code and switch the directory:
```bash
ln -s /var/www/virtual/$USER ~/site
cd ~/site
```
4. Clone the repository:
```bash
git clone https://gitlab.com/Tungmar/heroeshands.git .
```
5. Do the setup:
```bash
composer install
rmdir html
ln -s webroot html
cp settings.php.dist settings.php
cat ~/.my.cnf
```
6. Now you should see
```
[client]
user=your_user_name
password=your_password
```
Edit settings.php and replace *your_user_name* and *your_password* with your real credentials. *Notice* that the database name and part of the urlBase is the same as your_user_name.

7. Setup the database
```bash
vendor/bin/doctrine orm:schema-tool:create
bin/setupEntries.php
```

## Commands to moderate games
```
mysql -e "DELETE FROM cards WHERE dropStack = 'discardPile';"
```

## Helpfull commands

```
# Show all data in database
mysql -e "SELECT * FROM games; SELECT * FROM matches; SELECT * FROM players; SELECT * FROM cards;"

# Delete all data from database
mysql -e "DELETE FROM cards; DELETE FROM players; DELETE FROM matches; DELETE FROM games;"

# Create auth headers for Postman
mysql -se "SELECT concat('//x-auth:', p.auth, ' // ', g.id, ' ', g.name, ' - ', p.id, ' ', p.name) FROM players p LEFT JOIN matches m ON (p.match = m.id) LEFT JOIN games g ON (m.game = g.id);"
mysql -se "SELECT concat('//x-auth:', p.auth) FROM players p LEFT JOIN matches m ON (p.match = m.id) LEFT JOIN games g ON (m.game = g.id);"
```