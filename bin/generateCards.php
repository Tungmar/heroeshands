#!/usr/bin/php
<?php
declare(strict_types=1);

$sourceD = $argv[1] ?? null;
$targetD = 'Deck';

if (!file_exists($targetD)) {
    mkdir($targetD);
}

if (!$sourceD || !is_dir($sourceD) || !is_dir($targetD)) {
    die('Usage: '.$argv[0].' <source dir>');
}
var_dump($sourceD, $targetD);

$files = explode("\n",`find "$sourceD" -type f`);

foreach ($files as $file) {
    $path = explode('/', $file);
    if (isset($path[2])){
        $dir = $path[1];
        $name = $path[2];
    } elseif (isset($path[1])) {
        $dir = '.';
        $name = $path[1];
    } else {
        continue;
    }
    if ('Orig' === $dir) {
        continue;
    }

    #var_dump($targetD.'/'.$dir.'/'.$name);
    if (!is_dir($targetD.'/'.$dir)) {
        mkdir($targetD.'/'.$dir);
    }

    if (!preg_match('#^(\d+)x_.*?\.(jpeg|png|jpg)$#', $name, $matches)) {
        echo "Error: $name\n";
        copy($file, $targetD.'/'.$dir.'/'.$name);
        continue;
    }
    $count = (int) $matches[1];
    $sufix = $matches[2];

    for ($i=0; $i < $count; $i++) {
        echo $name."\n";
        $deck = $dir.'_';
        if ('.' === $dir) {
            $deck = 'Weiss_';
        }
        copy($file, $targetD.'/'.$dir.'/'.$deck.UUIDv4().'.'.$sufix);
    }
}


function UUIDv4() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0x0fff) | 0x4000,
        mt_rand(0, 0x3fff) | 0x8000,
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}
