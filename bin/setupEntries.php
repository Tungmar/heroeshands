#!/usr/bin/php
<?php
declare(strict_types=1);

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use UMA\DIC\Container;
use App\Controller\GameController;
use App\Domain\Card;
use App\Domain\Game;
use App\Domain\Match;
use App\Domain\Player;

define('APP_ROOT', dirname(__DIR__) . '/');

$container = require APP_ROOT . 'app/bootstrap.php';

$container->register(new App\Service\Doctrine());

$em = $container->get(EntityManager::class);


$players = [
    'Player1' => false,
    'Player2' => false,
    'Player3' => false,
    'Host' => true,
];

$gameController = new GameController($container);
$games = $gameController->getGames();

foreach ($games as $gameInfos) {
    $startCards = $gameInfos['config']['startCards'];
    echo "\nCreating game '".$gameInfos['config']['name']."'\n";

    $game = new Game($gameInfos['config']['name'], $gameInfos['directory']);
    $em->persist($game);
    $match = new Match($game->getName().': '.implode(' - ', array_keys($players)), $game);
    $em->persist($match);

    foreach ($players as $player => $isHost) {
        $entry = new Player($player, $match, $isHost);
        $auth = $entry->setAuth();
        $em->persist($entry);

        if (isset($startCards['allCards'])) {
            foreach ($startCards['allCards'] as $card) {
                $em->persist(new Card($entry, $card));
            }
        }

        if (isset($startCards['oneEachCards']) && [] !== $startCards['oneEachCards']) {
            $em->persist(new Card($entry, array_pop($startCards['oneEachCards'])));
        }

        if ($isHost) {
            if (isset($startCards['hostAllCards'])) {
                foreach ($startCards['hostAllCards'] as $card) {
                    $em->persist(new Card($entry, $card));
                }
            }
        }

        echo sprintf("%s: %splayer/%s\n", $player, $container->get('urlBase'), $auth);
    }

    $em->flush();
}

echo "\n";
