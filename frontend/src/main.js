// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vue2TouchEvents from 'vue2-touch-events'
import App from './App'
import router from './router'
import './../node_modules/bulma/css/bulma.css'

Vue.use(Vue2TouchEvents);

Vue.prototype.EventBus = new Vue();
window.BaseUrl = '/';

const axios = require('axios').default;

let auth = false;
router.beforeEach((to, from, next) => {
  if (to.name === 'NotFound' || to.path === '/') {
    console.log('NotFound is oki');
    next();
    return;
  }

  if (typeof to.params.auth !== 'undefined') {
    Vue.prototype.HTTP = axios.create({
      baseURL: window.BaseUrl + 'cheater/',
      headers: {
        'X-Auth': to.params.auth,
      }
    });
    auth = true;
  }

  if (auth) {
    console.log('yes yes');
    next();
  } else {
    console.log('no no');
    next({path: '/', params: {}});
  }
});

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
