import Vue from 'vue'
import Router from 'vue-router'
import Hand from '@/components/Hand'
import NotFound from '@/components/NotFound'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/dropStack/:dropStack',
      name: 'DropStack',
      component: Hand,
    },
    {
      path: '/player/:auth',
      name: 'Login',
      component: Hand,
    },
    {
      path: '/player',
      name: 'PlayersHand',
      component: Hand,
    },
    {
      path: '/',
      name: 'Home',
      component: NotFound,
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound,
    },
  ],
  linkActiveClass: 'is-active',
})
