<?php
declare(strict_types=1);

namespace App\Domain;

/**
 * A Match is a runnning game which use one Game and mulitple Players can participate.
 *
 * @Entity()
 * @Table(name="matches")
 */
class Match implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var Game
     *
     * @ManyToOne(targetEntity="Game")
     * @JoinColumn(name="game", nullable=false)
     */
    private $game;


    public function __construct(string $name, Game $game)
    {
        $this->name = $name;
        $this->game = $game;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getGame(): Game
    {
        return $this->game;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'game' => $this->getGame(),
        ];
    }
}
