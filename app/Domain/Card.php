<?php
declare(strict_types=1);

namespace App\Domain;

/**
 * @Entity()
 * @Table(name="cards",uniqueConstraints={@UniqueConstraint(name="player_path", columns={"player", "path"})})
 */
class Card implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    private $deckType;

    /**
     * @var Player
     *
     * @ManyToOne(targetEntity="Player")
     * @JoinColumn(name="player", nullable=false)
     */
    private $player;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    private $path;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    private $dropStack = null;

    /**
     * @var \DateTimeImmutable
     *
     * @Column(type="datetimetz_immutable", nullable=false)
     */
    private $createdAt;

    public function __construct(Player $player, string $path)
    {
        $this->deckType = basename(dirname($path));
        $this->player = $player;
        $this->path = $path;
        $this->createdAt = new \DateTimeImmutable('now');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDeckType(): string
    {
        return $this->deckType;
    }

    public function getPlayer(): Player
    {
        return $this->player;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getName(): string
    {
        return basename($this->path);
    }

    public function setPlayer(Player $player): void
    {
        $this->player = $player;
    }

    public function setDropStack(?string $dropStack): void
    {
        $this->dropStack = $dropStack;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'deckType' => $this->getDeckType(),
            'path' => $this->getPath(),
        ];
    }
}
