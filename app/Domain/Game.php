<?php
declare(strict_types=1);

namespace App\Domain;

/**
 * A Game define which decks, cards and dropstacks the game has.
 *
 * @Entity()
 * @Table(name="games")
 */
class Game implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    private $directory;


    public function __construct(string $name, string $directory)
    {
        $this->name = $name;
        $this->directory = $directory;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDirectory(): string
    {
        return $this->directory;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'directory' => $this->getDirectory(),
        ];
    }
}
