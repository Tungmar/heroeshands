<?php
declare(strict_types=1);

namespace App\Domain;

/**
 * @Entity()
 * @Table(name="players")
 */
class Player implements \JsonSerializable
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @Column(type="boolean", nullable=false)
     */
    private $isHost;

    /**
     * @var string
     *
     * @Column(type="string", nullable=false)
     */
    private $auth;

    /**
     * @var Match
     *
     * @ManyToOne(targetEntity="Match")
     * @JoinColumn(name="`match`", nullable=false)
     */
    private $match;


    public function __construct(string $name, Match $match, bool $isHost = false)
    {
        $this->name = $name;
        $this->match = $match;
        $this->isHost = $isHost;
    }

    public function setAuth(){
        $this->auth = $this->createToken(23);
        return $this->auth;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getIsHost(): bool
    {
        return $this->isHost;
    }

    public function getMatch(): Match
    {
        return $this->match;
    }

    public function getGame(): Game
    {
        return $this->match->getGame();
    }

    public function authVerify(string $auth): bool
    {
        return $auth === $this->auth;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
        ];
    }

    /**
     * Creates a cryptographically secure random string.
     * @author Scott
     * @see https://stackoverflow.com/a/13733588/1629120
     * @param int $length
     * @return string
     */
    protected function createToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }
}
