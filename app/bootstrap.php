<?php
declare(strict_types=1);

use UMA\DIC\Container;

require_once APP_ROOT . 'vendor/autoload.php';

$container = new Container();
$settings = require APP_ROOT . 'settings.php';
$settings($container);

return $container;
