<?php
declare(strict_types=1);

namespace App\Controller;

use APP\Domain\Game;
use UMA\DIC\Container;

class GameController
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var type
     */
    protected $pathGames;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->pathGames = $this->container->get('pathWebroot').$this->container->get('pathGames');
    }

    public function getGames(): array
    {
        $games = [];

        foreach ($this->readDir($this->pathGames) as $dir) {
            $file = $this->pathGames.$dir.'/game.json';

            if (!is_readable($file)) {
                continue;
            }

            $games[] = [
                'directory' => $dir,
                'config' => $this->readJson($file),
            ];
        }

        return $games;
    }

    public function getGameConfig(Game $game): array
    {
        $directory = $this->container->get('pathWebroot').$this->container->get('pathGames').$game->getDirectory();
        $file = $directory.'/game.json';
        if (!is_file($file) || !is_readable($file)) {
            throw new \RuntimeException('game.json is not readable: '.$file);
        }
        return $this->readJson($file);
    }

    public function getAllCardsForDeck(string $deck, Game $game): array
    {
        $config = $this->getGameConfig($game);
        $this->validateDeck($deck, $config);

        $directory = $this->container->get('pathGames').$game->getDirectory().'/'.$deck.'/';

        $files = scandir($this->container->get('pathWebroot').$directory, SCANDIR_SORT_NONE);
        if (!is_array($files) || 2 >= count($files)) {
            throw new \RuntimeException('Could not read cards from deck "%s"', $deck);
        }
        $files = array_filter($files, function($f) {
            return 0 !== strpos($f, '.');
        });
        array_walk($files, function(&$f) use ($directory) {
            $f = $directory.$f;
        });

        return $files;
    }

    private function validateConfig($config): void
    {
        $missing = [];
        $requiredList = ['name' => 'string', 'decks' => 'array', 'dropStacks' => 'array'];

        foreach ($requiredList as $required => $type) {
            if (!isset($config[$required]) || $type !== gettype($config[$required])) {
                $missing[] = $required;
            }
        }

        if ([] !== $missing) {
            throw new \RuntimeException('game.json is invalid. Missin or wrong: '.implode(', ', $missing));
        }
    }

    private function validateDeck(string $deck, $config): void
    {
        if (!array_key_exists($deck, $config['decks'])) {
            throw new \InvalidArgumentException(sprintf('Game has no deck "%s"', $deck));
        }
    }

    private function readDir($dir): array
    {
        $files = scandir($dir, SCANDIR_SORT_NONE);
        if (!is_array($files) || 2 >= count($files)) {
            throw new \RuntimeException(sprintf('Could not read "%s"', $dir));
        }
        $files = array_filter($files, function($f) {
            return 0 !== strpos($f, '.');
        });
        return $files;
    }

    private function readJson($file): array
    {
        $config = json_decode(file_get_contents($file), true);
        $this->validateConfig($config);
        return $config;
    }

}
