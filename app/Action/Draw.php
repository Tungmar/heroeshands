<?php
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ResponseInterface as Response;
use App\Domain\Card;
use App\Domain\Match;
use App\Domain\Player;

class Draw extends Action
{
    protected function process(): Response
    {
        $player = $this->loadPlayer();
        if (!$player) {
            return $this->respondError('Invalid bounds', 403);
        }

        $files = $this->gameController->getAllCardsForDeck($this->requestArgs['type'], $player->getGame());
        $files = array_flip($files);

        $this->em->getConnection()->beginTransaction();

        try {
            $qb = $this->em->createQueryBuilder();
            $qb->select('c')
                ->from(Card::class, 'c')
                ->leftJoin(Player::class, 'p', 'WITH', 'c.player = p')
                ->where('p.match = :match')
                ->andWhere('c.deckType = :deckType')
                ->setParameter('match', $player->getMatch())
                ->setParameter('deckType', $this->requestArgs['type'])
            ;
            #$this->debug['query'] = $qb->getQuery()->getSQL();
            $drawnCards = $qb->getQuery()->getResult();

            foreach ($drawnCards as $card) {
                unset($files[$card->getPath()]);
            }

            if ([] === $files) {
                return $this->respondError('Deck is empty!', 404);
            }

            $drawn = new Card($player, array_rand($files));
            $this->em->persist($drawn);

            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->debug['errMess'] = $e->getMessage();
            return $this->respondError('Card Draw Error', 400);
        }

        $response['card'] = $drawn;

        return $this->respond($response);
    }
}
