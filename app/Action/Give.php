<?php
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Domain\Card;
use App\Domain\Player;

class Give extends Action
{
    protected function process(): Response
    {
        $player = $this->loadPlayer();
        if (!$player) {
            return $this->respondError('Invalid bounds', 422);
        }

        $params = $this->request->getQueryParams();
        $path = preg_replace('#[^a-z0-9_/\.-]*#i', '', str_replace('|', '/', $this->requestArgs['path']));
        $to = preg_replace('#[^a-z0-9öäüéàè_-]*#i', '', $this->requestArgs['to']);
        $from = isset($params['from']) ? preg_replace('#[^a-z0-9_-]*#i', '', $params['from']) : false;
        $this->debug['path'] = $path;
        $this->debug['dropStack'] = $dropStack;
        $this->debug['to'] = $this->requestArgs['to'];
        $this->debug['from'] = $from;

        $this->em->getConnection()->beginTransaction();

        try {
            if (false === $from) {
                $query = ['player' => $player, 'path' => $path, 'dropStack' => null];
            } else {
                $query = ['path' => $path, 'dropStack' => $from];
            }
            $card = $this->em
                -> getRepository(Card::class)
                -> findBy($query)[0];

            $this->debug['card'] = $card;
            if (null === $card) {
                return $this->respondError('You don\'t have this card!', 422);
            }

            $other = $this->em
                -> getRepository(Player::class)
                -> findBy(['name' => $to, 'match' => $player->getMatch()])[0];
            if (null === $other) {
                return $this->respondError('There is no such player!', 422);
            }

            $card->setPlayer($other);
            $card->setDropStack(null);

            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $this->debug['errMess'] = $e->getMessage();
            return $this->respondError('Card Give Error', 400);
        }

        $response['card'] = $card;

        return $this->respond($response);
    }
}
