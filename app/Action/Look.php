<?php
declare(strict_types=1);

namespace App\Action;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Domain\Card;
use App\Domain\Player;

class Look extends Action
{
    protected function process(): Response
    {
        $player = $this->loadPlayer();
        if (!$player) {
            return $this->respondError('Invalid bounds', 422);
        }

        $response['player'] = $player->getName();
        $response['isHost'] = $player->getIsHost();

        $response['gameConfig'] = $this->gameController->getGameConfig($player->getGame());

        $dropStack = preg_replace('#[^a-z0-9_/.-]*#i', '', $this->requestArgs['dropStack']);
        if (empty($dropStack)) {
            $query = ['player' => $player, 'dropStack' => null];
        } else {
            $query = ['dropStack' => $dropStack];
        }
        $cards = $this->em
            -> getRepository(Card::class)
            -> findBy($query);
        $response['cards'] = $cards;


        $players = $this->em
            -> getRepository(Player::class)
            -> findBy(['match' => $player->getMatch()]);
        $response['players'] = $players;


        return $this->respond($response);
    }
}
