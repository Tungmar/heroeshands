<?php
declare(strict_types=1);

namespace App\Action;

use Doctrine\ORM\EntityManager;
use UMA\DIC\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Domain\Player;
use App\Controller\GameController;

abstract class Action
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var type GameController
     */
    protected $gameController;

    /**
     * @var Request
     */
    protected $request;

    protected $requestArgs;

    /**
     * @var Response
     */
    protected $response;

    protected $debug = [];

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $container->get(EntityManager::class);
        $this->gameController = $container->get(GameController::class);
    }

    public function __invoke(Request $request, Response $response, $args): Response
    {
        $this->request = $request;
        $this->requestArgs = $args;
        $this->response = $response;
        return $this->process();
    }

    abstract protected function process(): Response;

    protected function respond($data): Response
    {
        $body = [
            'status' => 'success',
            'data' => $data,
        ];
        if([] !== $this->debug && $this->container->get('develop_mode')){
            $body['debug'] = $this->debug;
        }
        return $this->sendResponse($body, 200);
    }

    protected function respondError($error, int $status = 500): Response
    {
        $body = [
            'status' => 'error',
            'error' => $error,
        ];
        if([] !== $this->debug && $this->container->get('develop_mode')){
            $body['debug'] = $this->debug;
        }
        return $this->sendResponse($body, $status);
    }

    private function sendResponse(array $body, int $status): Response
    {
        $body = \json_encode($body,
            $this->container->get('develop_mode') ? JSON_PRETTY_PRINT : 0
        );
        $this->response->getBody()->write($body);
        return $this->response
            -> withStatus($status)
            -> withHeader('Content-Type', 'application/json')
            -> withHeader('Content-Length', strlen($body))
        ;
    }

    protected function loadPlayer(): ?Player
    {
        $headers = $this->request->getHeaders();

        $player = $this->em
            -> getRepository(Player::class)
            -> findBy(['auth' => $headers['X-Auth'][0] ?? ''])[0];

        if (!$player || !$player->authVerify($headers['X-Auth'][0])) {
            return null;
        }

        return $player;
    }

}
