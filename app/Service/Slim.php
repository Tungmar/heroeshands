<?php
declare(strict_types=1);

namespace App\Service;

use Slim\App;
use Slim\Factory\AppFactory;
use UMA\DIC\Container;
use UMA\DIC\ServiceProvider;

class Slim implements ServiceProvider
{
    public function provide(Container $container): void
    {
        $container->set(App::class, static function (Container $container): App {
            $settings = $container->get('slim');

            $app = AppFactory::create(null, $container);

            $app->addErrorMiddleware(
                $settings['displayErrorDetails'],
                $settings['logErrors'],
                $settings['logErrorDetails']
            );

            return $app;
        });
    }
}
