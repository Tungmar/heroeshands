<?php
declare(strict_types=1);

namespace App\Service;

use UMA\DIC\Container;
use UMA\DIC\ServiceProvider;
use App\Controller\GameController;

class Controller implements ServiceProvider
{
    public function provide(Container $container): void
    {
        $container->set(GameController::class, static function (Container $container): GameController
        {
            return new GameController($container);
        });
    }
}
