<?php
declare(strict_types=1);

namespace App\Service;

use App\Action\Look;
use App\Action\Drop;
use App\Action\Draw;
use App\Action\Give;
use Slim\App;
use UMA\DIC\Container;
use UMA\DIC\ServiceProvider;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class SlimRoute implements ServiceProvider
{
    public function provide(Container $container): void
    {
        $app = $container->get(App::class);

        $app->get('/cheater[/]', function (Request $request, Response $response, $args) {
            $response->getBody()->write('Hello Cheater!');
            return $response;
        });

        $container->set(Look::class, static function(Container $container): Look {
            return new Look($container);
        });
        $app->get('/cheater/look[/{dropStack}]', Look::class);

        $container->set(Draw::class, static function(Container $container): Draw {
            return new Draw($container);
        });
        $app->get('/cheater/draw/{type}', Draw::class);

        $container->set(Drop::class, static function(Container $container): Drop {
            return new Drop($container);
        });
        $app->get('/cheater/drop/{path}[/{dropStack}]', Drop::class);

        $container->set(Give::class, static function(Container $container): Give {
            return new Give($container);
        });
        $app->get('/cheater/give/{path}/{to}', Give::class);
    }
}
