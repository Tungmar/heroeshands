<?php
declare(strict_types=1);

namespace App\Service;

use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use UMA\DIC\Container;
use UMA\DIC\ServiceProvider;

class Doctrine implements ServiceProvider
{
    public function provide(Container $container): void
    {
        $container->set(EntityManager::class, static function (Container $container): EntityManager {
            $develop_mode = $container->get('develop_mode');
            $settings = $container->get('doctrine');

            $ormConfig = Setup::createAnnotationMetadataConfiguration(
                $settings['metadata_dirs'],
                $develop_mode,
                null,
                $develop_mode
                    ? null
                    : new FilesystemCache($settings['cache_dir'])
            );

            return EntityManager::create(
                $settings['connection'],
                $ormConfig
            );
        });
    }
}
