<?php
declare(strict_types=1);

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use UMA\DIC\Container;

define('APP_ROOT', __DIR__ . '/');

$container = require APP_ROOT . 'app/bootstrap.php';

$container->register(new App\Service\Doctrine());

return ConsoleRunner::createHelperSet($container->get(EntityManager::class));
